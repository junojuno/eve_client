package com.cdp.eve;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.cdp.eve.activity.NotificationActivity_;
import com.cdp.eve.etc.Pref;
import com.cdp.eve.widget.EveWidgetProvider;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by junojuno on 2015. 6. 8..
 */
public class GcmService extends IntentService {
    public static final int NOTIFICATION_ID = 1;

    public GcmService() {
        super("GcmService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
//
        String messageType = gcm.getMessageType(intent);
        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                int level = Integer.parseInt(intent.getStringExtra("level"));
                int alarmIdx = Integer.parseInt(intent.getStringExtra("alarm_idx"));
                int cntAlarms = Integer.parseInt(intent.getStringExtra("cnt_alarms"));
                String img = intent.getStringExtra("img");

                SharedPreferences.Editor editor = Pref.init(this).edit();
                if (level != 0) {
                    editor.putInt(Pref.LATEST_LEVEL, level);
                }
                editor.putInt(Pref.LATEST_CNT_ALARMS, cntAlarms);
                editor.commit();


                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
                int[] ids = appWidgetManager.getAppWidgetIds(new ComponentName(this, EveWidgetProvider.class));

                Intent intentWidget = new Intent(this, EveWidgetProvider.class);
                intentWidget.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                intentWidget.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
                sendBroadcast(intentWidget);

                NotificationActivity_.intent(getApplicationContext()).
                        flags(Intent.FLAG_ACTIVITY_NEW_TASK).imgUrl(img).alarmIdx(alarmIdx).
                        level(level).start();

            }
        }

        GcmReceiver.completeWakefulIntent(intent);
    }
}
