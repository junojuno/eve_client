package com.cdp.eve.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cdp.eve.R;
import com.cdp.eve.activity.MainActivity;
import com.cdp.eve.activity.PhotoDetailActivity;
import com.cdp.eve.activity.PhotoDetailActivity_;
import com.cdp.eve.adapter.BlacklistAdapter;
import com.cdp.eve.etc.EveCircleTransformation;
import com.cdp.eve.etc.ImageBarChartRenderer;
import com.cdp.eve.etc.TextCounter;
import com.cdp.eve.etc.TypefaceManager;
import com.cdp.eve.model.Alarm;
import com.cdp.eve.network.API;
import com.cdp.eve.network.EveResponse;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ValueFormatter;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.ViewsById;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import it.sephiroth.android.library.widget.AdapterView;
import it.sephiroth.android.library.widget.HListView;


@EFragment(R.layout.fragment_analysis)
public class AnalysisFragment extends BaseFragment {

    int cntLoadedImg = 0;

    @ViewById(R.id.svAnalysis)
    ScrollView svAnalysis;

    @ViewById(R.id.pcAnalysisWeeklyReportFreq)
    PieChart pcFrequency;

    @ViewById(R.id.bcAnalysisWeeklyReportPeriod)
    HorizontalBarChart bcPeriod;

    @ViewById(R.id.hlvAnalysisBlacklist)
    HListView hlvBlacklist;

    @ViewById(R.id.tvAnalysisWeeklyReportTotal)
    TextView tvWeeklyTotal;

    @ViewById(R.id.tvAnalysisWeeklyReportVibrationCnt)
    TextView tvVibrationTotal;

    @ViewById(R.id.tvAnalysisWeeklyReportTitle)
    TextView tvTitle;

    @ViewById(R.id.tvAnalysisBlacklistTitle)
    TextView tvBlacklist;

    @ViewById(R.id.tvAnalysisWeeklyReportClick)
    TextView tvClick;

    @ViewById(R.id.tvAnalysisWeeklyReportFreqTitle)
    TextView tvVisitors;

    @ViewsById({R.id.tvAnalysisWeeklyReportLevel1, R.id.tvAnalysisWeeklyReportLevel2,
            R.id.tvAnalysisWeeklyReportLevel3, R.id.tvAnalysisWeeklyReportLevel4,
            R.id.tvAnalysisWeeklyReportLevel5})
    List<TextView> tvWeeklLevels;

    @ViewById(R.id.tvAnalysisWeeklyReportTotalTitle)
    TextView tvTotalTitle;

    @ViewById(R.id.tvAnalysisWeeklyReportVibrationTitle)
    TextView tvVibrationTitle;



    @ViewById(R.id.tvAnalysisVisitorsTitle)
    TextView tvVisitorsTitle;

    @ViewsById({R.id.lcAnalysisVisitors1, R.id.lcAnalysisVisitors2, R.id.lcAnalysisVisitors3})
    List<LineChart> lcVisitors;

    @ViewsById({R.id.tvAnalysisVisitorsStartTime1, R.id.tvAnalysisVisitorsStartTime2, R.id.tvAnalysisVisitorsStartTime3})
    List<TextView> tvVisitorStarts;

    @ViewsById({R.id.tvAnalysisVisitorsEndTime1, R.id.tvAnalysisVisitorsEndTime2, R.id.tvAnalysisVisitorsEndTime3})
    List<TextView> tvVisitorEnds;

//    @ViewById(R.id.lcAnalysisVisitors)
//    LineChart lcVisitors;
//
//    @ViewById(R.id.tvAnalysisVisitorsStartTime)
//    TextView tvVisitorsStartTime;
//
//    @ViewById(R.id.tvAnalysisVisitorsEndTime)
//    TextView tvVisitorsEndTime;

    private ArrayList<Alarm> alarms;
    Map<Integer, Integer> periods;
    int[] topRated;

//    @ItemClick(R.id.hlvAnalysisBlacklist)
//    public void blacklistSelect(int position) {
//        Log.e("selected BlackList", position + "");
//    }


    @Click({R.id.tvAnalysisWeeklyReportClick, R.id.lcAnalysisVisitors1, R.id.lcAnalysisVisitors2, R.id.lcAnalysisVisitors3})
    void clickFreq(View view) {
        ((MainActivity)getActivity()).onTabSelected(2);
    }

    @AfterViews
    void init () {
        alarms = ((MainActivity)getActivity()).getAlarms();
        if (alarms != null && !alarms.isEmpty()) {
            int bg = 0;

            if (alarms.size() <= 15) {
                bg = R.drawable.bg_lv1;
            } else if (alarms.size() <= 30) {
                bg = R.drawable.bg_lv2;
            } else if (alarms.size() <= 45) {
                bg = R.drawable.bg_lv3;
            } else if (alarms.size() <= 60) {
                bg = R.drawable.bg_lv4;
            } else {
                bg = R.drawable.bg_lv5;
            }

            svAnalysis.setBackgroundResource(bg);
        }

        initChart();
        hlvBlacklist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                PhotoDetailActivity_.intent(getActivity()).alarm(alarms.get(i)).alarms(alarms).start();
            }
        });
    }

    private void setDataWithAlarms() {
        setPieChart();
        setBarChart();
        setBlackList();
        setVisitors();
    }

    void setVisitors() {

        for (int j = 0 ; j < 3  ; j ++) {
            int key = topRated[2 - j];
            final LineChart chart = lcVisitors.get(j);
            TextView start = tvVisitorStarts.get(j);
            TextView end = tvVisitorEnds.get(j);

            start.setText(String.format("%02d:00", key));
            end.setText(String.format("%02d:00", key + 1));

            ArrayList<String> xVals = new ArrayList<String>();
            for (int i = 0; i < 4; i++) {
                xVals.add((i) + "");
            }

            ArrayList<Entry> yVals = new ArrayList<Entry>();
            ArrayList<Alarm> targetAlarms = new ArrayList<Alarm>();

            int idx = 0;
            Calendar cal = Calendar.getInstance();
            for (int i = 0; (i < alarms.size()) && idx < 4; i++) {
                Alarm alarm = alarms.get(i);
                cal.setTime(alarm.getEndTime());
                int hour = cal.get(Calendar.HOUR_OF_DAY);

                if (hour == key) {
                    targetAlarms.add(alarm);
                    yVals.add(new Entry(alarm.getLevel(), idx));
                    idx++;
                }
            }

            LineDataSet set1 = new LineDataSet(yVals, "DataSet 1");

            set1.setColor(Color.WHITE);
            set1.setLineWidth(1);

            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(set1); // add the datasets

            final LineData data = new LineData(xVals, dataSets);
            data.setDrawValues(false);

            setVisitorImgs(chart, targetAlarms, data);

        }

    }

    @Background
    void setVisitorImgs(LineChart chart, ArrayList<Alarm> targetAlarms, LineData data) {
        final List<Bitmap> visitors = new ArrayList<Bitmap>();
        for (int i = 0; i < targetAlarms.size(); i++) {
            final String img = targetAlarms.get(i).getImg();

            try {
                Bitmap bitmap = Picasso.with(getActivity()).load(img)
                        .transform(new EveCircleTransformation())
                        .resize(130, 130).get();

                visitors.add(bitmap);

                if (visitors.size() == targetAlarms.size()) {
                    chart.setRenderer(new ImageBarChartRenderer(
                            chart, chart.getAnimator(), chart.getViewPortHandler(), visitors, getActivity()));

                    chart.setData(data);
                    chart.notifyDataSetChanged();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private void setBlackList() {
        BlacklistAdapter blacklistAdapter = new BlacklistAdapter(getActivity(), alarms);
        hlvBlacklist.setAdapter(blacklistAdapter);
    }

    private Map sortByValue(Map unsortedMap) {
        List list = new LinkedList(unsortedMap.entrySet());

        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });

        Map sortedMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    private void setBarChart() {

        periods = new HashMap<Integer, Integer>();

        for (int i = 0 ; i < 24 ; i++) {
            periods.put(i, 0);
        }

        Calendar cal = Calendar.getInstance();

        for (Alarm alarm : alarms) {
            cal.setTime(alarm.getEndTime());

            int idx = cal.get(Calendar.HOUR_OF_DAY);
            Integer target = periods.get(idx) + 1;
            periods.put(idx, target);
        }

        periods = sortByValue(periods);

        for (int i = 0 ; i < 24 ; i++) {
        }

        List<String> xVals = new ArrayList<String>();
        List<BarEntry> yVals = new ArrayList<BarEntry>();

        topRated = new int[3];
        int i = 0;
        for (Integer key : periods.keySet()) {
            if (i == 3) {
                break;
            }
            topRated[3 - i - 1] = key;
            i++;
        }

        for (i = 0 ; i < 3 ; i++) {
            int key = topRated[i];
            Integer val = periods.get(key);
            xVals.add(String.format("%02d ~ %02d:00  ", key, key + 1));
            yVals.add(new BarEntry(val, i));
        }

        List<BarDataSet> barDataset = new ArrayList<BarDataSet>();
        BarDataSet set = new BarDataSet(yVals, "test");
        set.setBarSpacePercent(70);
        set.setDrawValues(false);

        List<Integer> colors = new ArrayList<Integer>();
        colors.add(getResources().getColor(R.color.color_lv2));
        colors.add(getResources().getColor(R.color.color_lv3));
        colors.add(getResources().getColor(R.color.color_lv4));
        set.setColors(colors);

        barDataset.add(set);
        BarData barData = new BarData(xVals, barDataset);
        barData.setValueTextColor(getResources().getColor(android.R.color.transparent));
        barData.setValueTextSize(12);

        bcPeriod.setData(barData);
        bcPeriod.invalidate();
    }

    private void setPieChart() {

        TextCounter.animateTextView(0, alarms.size(), tvWeeklyTotal);
        List<String> xVals = new ArrayList<String>();
        for (int i = 1 ; i <=5 ; i++) {
            xVals.add(i + "");
        }
        int frequencies[] = new int[6];
        int vibration = 0;
        for (int i = 0 ; i < alarms.size() ; i++) {
            Alarm alarm = alarms.get(i);
            frequencies[alarm.getLevel()] ++ ;
            if (alarm.isVibrate() == 1) {
                vibration ++;
            }
        }
        TextCounter.animateTextView(0, vibration, tvVibrationTotal);
//        tvVibrationTotal.setText(vibration + "");

        List<Entry> yVals = new ArrayList<Entry>();

        for (int i = 1 ; i <= 5 ; i++) {
            tvWeeklLevels.get(i - 1).setText(frequencies[i] + "");
            yVals.add(new Entry(frequencies[i], i - 1));
        }
        PieDataSet pieDataset = new PieDataSet(yVals, "Weekly Report");

        PieData pieData = new PieData(xVals, pieDataset);
        pieData.setDataSet(pieDataset);
        pieData.setDrawValues(false);

        ArrayList<Integer> colors = new ArrayList<Integer>();

        colors.add(getResources().getColor(R.color.color_lv1));
        colors.add(getResources().getColor(R.color.color_lv2));
        colors.add(getResources().getColor(R.color.color_lv3));
        colors.add(getResources().getColor(R.color.color_lv4));
        colors.add(getResources().getColor(R.color.color_lv5));
        pieDataset.setColors(colors);

        pcFrequency.setData(pieData);
        pcFrequency.invalidate();
    }

    private void initChart() {
        Typeface semibold = TypefaceManager.getInstance(getActivity()).getTypeface(TypefaceManager.TYPEFACE_SEMIBOLD);
        Typeface regular = TypefaceManager.getInstance(getActivity()).getTypeface(TypefaceManager.TYPEFACE_REGULAR);
        Typeface light = TypefaceManager.getInstance(getActivity()).getTypeface(TypefaceManager.TYPEFACE_LIGHT);

        tvTitle.setTypeface(semibold);
        tvClick.setTypeface(regular);

        tvTotalTitle.setTypeface(semibold);
        tvBlacklist.setTypeface(semibold);
        tvVibrationTitle.setTypeface(semibold);
        tvVisitorsTitle.setTypeface(semibold);

        for (int i = 0; i < 3 ; i++) {
            tvVisitorStarts.get(i).setTypeface(regular);
            tvVisitorEnds.get(i).setTypeface(regular);
        }

        tvVisitors.setTypeface(semibold);

        tvWeeklyTotal.setTypeface(light);
        tvVibrationTotal.setTypeface(light);

        for (int i = 0 ; i < 5 ; i ++) {
            tvWeeklLevels.get(i).setTypeface(regular);
        }

        pcFrequency.setCenterText(null);
        pcFrequency.setDescription(null);
        pcFrequency.setHighlightEnabled(false);
        pcFrequency.setRotationEnabled(false);
        pcFrequency.setDrawHoleEnabled(false);
        pcFrequency.setDrawCenterText(false);
        pcFrequency.setDrawSliceText(false);
        pcFrequency.getLegend().setEnabled(false);
        pcFrequency.setTouchEnabled(false);
        pcFrequency.offsetLeftAndRight(0);
        pcFrequency.offsetTopAndBottom(0);
        pcFrequency.setPadding(0, 0, 0, 0);
        pcFrequency.animateY(1500);

        bcPeriod.setTouchEnabled(false);
        bcPeriod.setHighlightEnabled(false);
        bcPeriod.setDoubleTapToZoomEnabled(false);
        bcPeriod.setDrawBorders(false);
        bcPeriod.setDrawGridBackground(false);
        bcPeriod.setDrawMarkerViews(false);
        bcPeriod.setDescription(null);

        bcPeriod.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        bcPeriod.getXAxis().setTextSize(12);
        bcPeriod.getXAxis().setTextColor(getResources().getColor(R.color.white));
        bcPeriod.getXAxis().setDrawGridLines(false);
        bcPeriod.getXAxis().setDrawAxisLine(false);

        bcPeriod.getAxisLeft().setEnabled(false);
        bcPeriod.getAxisRight().setEnabled(false);
        bcPeriod.getLegend().setEnabled(false);
        bcPeriod.animateY(1500);

        for (int i = 0 ; i < 3; i++) {
            LineChart lc = lcVisitors.get(i);

            lc.setDescription("");
            lc.setNoDataTextDescription("You need to provide data for the chart.");

            lc.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            lc.setDrawGridBackground(false);
            lc.setTouchEnabled(true);
            lc.setDragEnabled(false);
            lc.setScaleEnabled(false);
            lc.setPinchZoom(false);
            lc.setScaleEnabled(false);
            lc.setDoubleTapToZoomEnabled(false);
            lc.getLegend().setEnabled(false);
            lc.setViewPortOffsets(100, 100, 100, 100);
            lc.setHighlightEnabled(false);

            YAxis leftAxis = lc.getAxisLeft();
            leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
            leftAxis.setSpaceTop(100f);
            leftAxis.setSpaceTop(0f);

            leftAxis.setAxisMaxValue(5.0f);
            leftAxis.setAxisMinValue(1.0f);
            leftAxis.setStartAtZero(true);
            leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
            leftAxis.isDrawAxisLineEnabled();
            leftAxis.setTextColor(Color.TRANSPARENT);
            leftAxis.setGridColor(Color.TRANSPARENT);
            leftAxis.setAxisLineColor(Color.TRANSPARENT);
            leftAxis.setLabelCount(4);
            leftAxis.setDrawAxisLine(false);
            leftAxis.setDrawLabels(false);
            leftAxis.setValueFormatter(new ValueFormatter() {
                @Override
                public String getFormattedValue(float value) {
                    int level = ((int) value);

                    if (level == 0) {
                        return "";
                    } else {
                        return "Lv." + level;
                    }
                }
            });

            lc.getXAxis().setEnabled(false);
            lc.getAxisRight().setEnabled(false);
            lc.getAxisLeft().setDrawAxisLine(false);

            Legend l = lc.getLegend();
            l.setForm(Legend.LegendForm.LINE);


        }

        setDataWithAlarms();
    }

    @Override
    public void invalidate() {
        if (lcVisitors != null) {
            for (int i = 0; i < 3; i++) {
                lcVisitors.get(i).animateY(500);
//                lcVisitors.get(i).setVisibility(View.GONE);
//                lcVisitors.get(i).setVisibility(View.VISIBLE);
//                lcVisitors.get(i).invalidate();
//                lcVisitors.get(i).notifyDataSetChanged();
            }
        }
//        bcPeriod.invalidate();
//        pcFrequency.invalidate();
//        lcVisitors.invalidate();
    }
}
