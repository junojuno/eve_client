package com.cdp.eve.fragment.guide;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.cdp.eve.R;
import com.cdp.eve.fragment.BaseFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;


@EFragment(R.layout.fragment_guide_start)
public class GuideStartFragment extends BaseFragment {
    private static final int PAGE_NUM = 0;

    @AfterViews
    void init() {

    }

    @Override
    public void invalidate() {

    }
}
