package com.cdp.eve.fragment;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.GridView;

import com.cdp.eve.R;
import com.cdp.eve.activity.MainActivity;
import com.cdp.eve.activity.PhotoDetailActivity_;
import com.cdp.eve.adapter.PhotosAdapter;
import com.cdp.eve.model.Alarm;
import com.cdp.eve.network.API;
import com.cdp.eve.network.EveResponse;
import com.cdp.eve.view.FilterDialog;
import com.cdp.eve.view.FilterDialogCompleteResponse;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


@EFragment(R.layout.fragment_photos)
public class PhotosFragment extends BaseFragment implements FilterDialogCompleteResponse {

    @ViewById(R.id.gvPhotos)
    GridView gvPhotos;

    private ArrayList<Alarm> alarms;

    PhotosAdapter photosAdapter;

    private boolean isInnerOfDateTimeRange(Alarm alarm, Date start, Date end) {
        boolean isAfter = alarm.getEndTime().after(start);
        boolean isBefore = alarm.getEndTime().before(end);
        return  isAfter && isBefore;
    }

    @AfterViews
    void init () {
        ArrayList<Alarm> unalignedAlarms = ((MainActivity) getActivity()).getAlarms();
        alarms = new ArrayList<Alarm>();

        for (int i = unalignedAlarms.size() - 1 ; i >= 0 ; i--) {
            alarms.add(unalignedAlarms.get(i));
        }

        setHasOptionsMenu(true);
        initPhotos();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                new FilterDialog(getActivity(), this).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @ItemClick(R.id.gvPhotos)
    void loadPhotoDetail(int pos) {
        PhotoDetailActivity_.intent(this).alarms(alarms).alarm(alarms.get(pos)).start();
    }

    private void initPhotos() {
        photosAdapter = new PhotosAdapter(getActivity(), alarms);
        gvPhotos.setAdapter(photosAdapter);
    }

    @Override
    public void onFilterAdjust(int level, int sY, int sMon, int sD, int sH, int sMin, int eY, int eMon, int eD, int eH, int eMin) {
        try {
            Log.e("level",level +"");
            String strStartDate = String.format("%04d-%02d-%02d %02d:%02d",
                    sY, sMon, sD, sH, sMin);
            Log.e("strStartDate",strStartDate);
            String strEndDate = String.format("%04d-%02d-%02d %02d:%02d",
                    eY, eMon, eD, eH, eMin);
            Log.e("strEndDate",strEndDate);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            Date startDate = format.parse(strStartDate);
            Date endDate = format.parse(strEndDate);

            ArrayList<Alarm> filteredAlarms = new ArrayList<Alarm>();

            for (Alarm alarm : alarms) {
                if ((level == 6 || alarm.getLevel() == level) &&
                        isInnerOfDateTimeRange(alarm, startDate, endDate)) {
                    filteredAlarms.add(alarm);
                }
            }

            photosAdapter = new PhotosAdapter(getActivity(), filteredAlarms);
            gvPhotos.setAdapter(photosAdapter);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void invalidate() {
        init();
    }
}
