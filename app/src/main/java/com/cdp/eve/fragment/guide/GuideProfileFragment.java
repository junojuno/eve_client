package com.cdp.eve.fragment.guide;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cdp.eve.R;
import com.cdp.eve.fragment.BaseFragment;

import org.androidannotations.annotations.EFragment;


@EFragment(R.layout.fragment_guide_profile)
public class GuideProfileFragment extends BaseFragment {

    @Override
    public void invalidate() {

    }
}
