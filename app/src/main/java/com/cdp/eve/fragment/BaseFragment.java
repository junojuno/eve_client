package com.cdp.eve.fragment;

import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {
    public BaseFragment() {
    }

    public abstract void invalidate();
}
