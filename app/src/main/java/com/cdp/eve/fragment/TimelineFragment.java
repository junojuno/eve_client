package com.cdp.eve.fragment;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.annotation.UiThread;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cdp.eve.R;
import com.cdp.eve.activity.MainActivity;
import com.cdp.eve.etc.Menter;
import com.cdp.eve.etc.TypefaceManager;
import com.cdp.eve.model.Alarm;
import com.cdp.eve.network.API;
import com.cdp.eve.network.EveResponse;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ValueFormatter;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import pl.droidsonroids.gif.GifTextView;

@EFragment(R.layout.fragment_timeline)
public class TimelineFragment extends BaseFragment {

    private static int CNT_LOAD_ONCE = 5;

    CountDownTimer timer;
    Alarm centeredAlarm;

    @ViewById(R.id.llTimeLineBg)
    LinearLayout llBg;

    @ViewById(R.id.tvTimeLineDate)
    TextView tvDate;

    @ViewById(R.id.tvTimeLineTime)
    TextView tvTime;

    @ViewById(R.id.lcStatis)
    LineChart mChart;

    @ViewById(R.id.civTimeLinePic)
    CircleImageView ivPic;

    @ViewById(R.id.ivTimelineLeft)
    ImageView ivLeft;

    @ViewById(R.id.ivTimelineRight)
    ImageView ivRight;

    @ViewById(R.id.givMainEve)
    GifTextView givEve;

    @ViewById(R.id.tvMainEveMent)
    TextView tvMainEveMent;

    private ArrayList<Alarm> alarms;

    int target = 0;

    @AfterViews
    void init() {
        alarms = ((MainActivity)getActivity()).getAlarms();
        tvDate.setTypeface(TypefaceManager.getInstance(getActivity()).getTypeface(TypefaceManager.TYPEFACE_LIGHT));
        tvTime.setTypeface(TypefaceManager.getInstance(getActivity()).getTypeface(TypefaceManager.TYPEFACE_REGULAR));
        tvMainEveMent.setTypeface(TypefaceManager.getInstance(getActivity()).getTypeface(TypefaceManager.TYPEFACE_REGULAR));
        initChart();
    }

    @Click({R.id.ivTimelineLeft, R.id.ivTimelineRight})
    void moveToLeftOrRight(View view) {

        switch (view.getId()) {
            case R.id.ivTimelineLeft:
                if (target > CNT_LOAD_ONCE) {
                    target--;
                }
                break;
            case R.id.ivTimelineRight:
                if (target < alarms.size() - 1) {
                    target++;
                }
                break;
        }
        setDataWithAlarms(target);
        stopMent();
        startMent();
    }

    private void initChart() {
        mChart.setDescription("");
        mChart.setNoDataTextDescription("You need to provide data for the chart.");

        mChart.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        mChart.setDrawGridBackground(false);
        mChart.setTouchEnabled(true);
        mChart.setDragEnabled(false);
        mChart.setScaleEnabled(false);
        mChart.setPinchZoom(false);
        mChart.setScaleEnabled(false);
        mChart.setDoubleTapToZoomEnabled(false);
        mChart.getLegend().setEnabled(false);
        mChart.setViewPortOffsets(0, 50, 0, 0);


        mChart.setHighlightEnabled(false);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.setSpaceTop(100f);
        leftAxis.setSpaceTop(0f);

        leftAxis.setAxisMaxValue(5.0f);
        leftAxis.setAxisMinValue(1.0f);
        leftAxis.setStartAtZero(true);

        leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);


        leftAxis.isDrawAxisLineEnabled();
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setGridColor(Color.WHITE);
        leftAxis.setAxisLineColor(Color.WHITE);
        leftAxis.setLabelCount(4);

        leftAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                int level = ((int) value);

                if (level == 0) {
                    return "";
                } else {
                    return "Lv." + level;
                }
            }
        });

        target = alarms.size() - 1;
        setDataWithAlarms(target);
        mChart.getXAxis().setEnabled(false);
        mChart.getAxisRight().setEnabled(false);
        mChart.getAxisLeft().setDrawAxisLine(false);
        mChart.setVisibleXRange(CNT_LOAD_ONCE - 1);

        Legend l = mChart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
    }

    @UiThread
    void changeEveMent(String ment) {
        tvMainEveMent.setText(ment);
    }

    private void setDataWithAlarms(int latest) {
        int start = latest - CNT_LOAD_ONCE + 1;
        int end = latest;
        int center = latest - (CNT_LOAD_ONCE / 2);

        if (start < 0 || end > alarms.size()) {
            return ;
        }

        centeredAlarm = alarms.get(center);

        SimpleDateFormat sdfDateMon = new SimpleDateFormat("MMMM", Locale.ENGLISH);
        SimpleDateFormat sdfDateDay = new SimpleDateFormat("d", Locale.ENGLISH);

        SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm");

        String strDate =
                sdfDateMon.format(centeredAlarm.getEndTime()).substring(0, 3)
                        + " "
                        + sdfDateDay.format(centeredAlarm.getEndTime());

        //asset file
        try {
            String eveGif = "";

            switch (centeredAlarm.getLevel()) {
                case 1:
                    eveGif = "eve_ment_lv1.gif";
                    break;
                case 2:
                    eveGif = "eve_ment_lv2.gif";
                    break;
                case 3:
                    eveGif = "eve_ment_lv3.gif";
                    break;
                case 4:
                    eveGif = "eve_ment_lv4.gif";
                    break;
                case 5:
                    eveGif = "eve_ment_lv5.gif";
                    break;
            }

            GifDrawable gifFromAssets = new GifDrawable(getActivity().getAssets(), eveGif);
            givEve.setBackground(gifFromAssets);

        } catch (IOException e) {
            e.printStackTrace();
        }

        tvDate.setText(strDate);
        tvTime.setText(sdfTime.format(centeredAlarm.getEndTime()));

        int bg = 0;
        switch (centeredAlarm.getLevel()) {
            case 1:
                bg = R.drawable.bg_lv1;
                break;
            case 2:
                bg = R.drawable.bg_lv2;
                break;
            case 3:
                bg = R.drawable.bg_lv3;
                break;
            case 4:
                bg = R.drawable.bg_lv4;
                break;
            case 5:
                bg = R.drawable.bg_lv5;
                break;
        }


        llBg.setBackgroundResource(bg);

        if (centeredAlarm.getLevel() != 1) {
            Picasso.with(getActivity()).load(centeredAlarm.getImg()).centerCrop().resize(420, 420).into(ivPic);
        } else {
            ivPic.setImageResource(R.mipmap.ic_timeline_lv1);
        }

        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < 5; i++) {
            xVals.add((i) + "");
        }

        ArrayList<Entry> yVals = new ArrayList<Entry>();
        for (int i = start ; i <= end ; i++) {
            Alarm alarm = alarms.get(i);
            yVals.add(new Entry(alarm.getLevel(), i - start));
        }

        LineDataSet set1 = new LineDataSet(yVals, "DataSet 1");

        set1.setColor(Color.WHITE);
        List<Integer> circleColors = new ArrayList<Integer>();

        int borderColor = getResources().getColor(R.color.chart_circle_lv1);

        switch (centeredAlarm.getLevel()) {
            case 1:
                borderColor = getResources().getColor(R.color.chart_circle_lv1);
                break;
            case 2:
                borderColor = getResources().getColor(R.color.chart_circle_lv2);
                break;
            case 3:
                borderColor = getResources().getColor(R.color.chart_circle_lv3);
                break;
            case 4:
                borderColor = getResources().getColor(R.color.chart_circle_lv4);
                break;
            case 5:
                borderColor = getResources().getColor(R.color.chart_circle_lv5);
                break;
        }

//        circleColors.add(borderColor);
//        circleColors.add(borderColor);
//        circleColors.add(getResources().getColor(R.color.white));
//        circleColors.add(borderColor);
//        circleColors.add(borderColor);
        circleColors.add(getResources().getColor(android.R.color.transparent));
        circleColors.add(getResources().getColor(android.R.color.transparent));
        circleColors.add(getResources().getColor(R.color.white));
        circleColors.add(getResources().getColor(android.R.color.transparent));
        circleColors.add(getResources().getColor(android.R.color.transparent));

        set1.setCircleColors(circleColors);
        set1.setCircleColorHole(Color.WHITE);

        set1.setFillColor(getResources().getColor(R.color.chart_fill_white));
        set1.setDrawFilled(true);

        set1.setLineWidth(1f);
        set1.setCircleSize(5f);

        set1.setDrawCircleHole(true);
        set1.setValueTextSize(9f);
        set1.setFillAlpha(65);

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1); // add the datasets

        LineData data = new LineData(xVals, dataSets);
        data.setDrawValues(false);
        // set data
        if (mChart.getData() != null) {
            mChart.clearValues();
        }
        mChart.setData(data);
        mChart.notifyDataSetChanged();
    }

    public void startMent() {
        mChart.animateY(700);
        final String ment = Menter.getInstance(getActivity()).getMentByRand(centeredAlarm.getLevel());
        timer = new CountDownTimer(30000, 100) {
            String str = "";
            int idx = 0;

            @Override
            public void onTick(long millisUntilFinished) {
                if (ment != null && !ment.isEmpty()) {
                    str += ment.substring(idx, idx + 1);
                    changeEveMent(str);
                    idx++;
                    if (idx >= ment.length()) {
                        cancel();
                    }
                }
            }

            @Override
            public void onFinish() {

            }
        };

        timer.start();
    }

    public void stopMent() {
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void invalidate() {
        init();
    }
}
