package com.cdp.eve.model;

import java.util.Date;

/**
 * Created by Administrator on 2015-07-13.
 */
public class Statis {
    private String imgSrc;
    private String ment;
    private Date date;
    private int level;
    private boolean isBookmarked;

    public Statis(String imgSrc, String ment, Date date, int level, boolean isBookmarked) {
        this.imgSrc = imgSrc;
        this.ment = ment;
        this.date = date;
        this.level = level;
        this.isBookmarked = isBookmarked;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public String getMent() {
        return ment;
    }

    public void setMent(String ment) {
        this.ment = ment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isBookmarked() {
        return isBookmarked;
    }

    public void setIsBookmarked(boolean isBookmarked) {
        this.isBookmarked = isBookmarked;
    }

}
