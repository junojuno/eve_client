package com.cdp.eve.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.Date;

/**
 * Created by junojuno on 2015. 8. 5..
 */
public class Alarm implements Parcelable{
    private int diff;
    private String img;
    private int level;
    private Date startTime;
    private Date endTime;
    private int isVibrate;

    public Alarm(int diff, String img, int level, Date startTime, Date endTime, int isVibrate) {
        this.diff = diff;
        this.img = img;
        this.level = level;
        this.startTime = startTime;
        this.endTime = endTime;
        this.isVibrate = isVibrate;
    }

    public int getDiff() {
        return diff;
    }

    public void setDiff(int diff) {
        this.diff = diff;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(diff);
        parcel.writeString(img);
        parcel.writeInt(level);
        parcel.writeSerializable(startTime);
        parcel.writeSerializable(endTime);
        parcel.writeInt(isVibrate);
    }

    protected Alarm(Parcel in) {
        diff = in.readInt();
        img = in.readString();
        level = in.readInt();
        startTime = (Date) in.readSerializable();
        endTime = (Date) in.readSerializable();
        isVibrate = in.readInt();
    }

    public static final Creator<Alarm> CREATOR = new Creator<Alarm>() {
        @Override
        public Alarm createFromParcel(Parcel in) {
            return new Alarm(in);
        }

        @Override
        public Alarm[] newArray(int size) {
            return new Alarm[size];
        }
    };

    public int isVibrate() {
        return isVibrate;
    }

    public void setIsVibrate(int isVibrate) {
        this.isVibrate = isVibrate;
    }
}
