package com.cdp.eve;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.cdp.eve.etc.Pref;

/**
 * Created by junojuno on 2015. 6. 8..
 */
public class GcmReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        boolean isRightMsg = false;
        for (String key : bundle.keySet())
        {
            Object value = bundle.get(key);

            if (key.equals("message")) {
                isRightMsg = true;
            }

            Log.e("onReceive", "|" + String.format("%s : %s (%s)", key, value.toString(), value.getClass().getName()) + "|");
        }
        boolean isGcmAble = Pref.init(context).getBoolean(Pref.GCM_ON, true);
        if (isGcmAble) {
            ComponentName comp = new ComponentName(context.getPackageName(), GcmService.class.getName());
            startWakefulService(context, intent.setComponent(comp));
            setResultCode(Activity.RESULT_OK);
        }

    }
}
