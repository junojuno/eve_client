package com.cdp.eve.etc;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by junojuno on 2015. 8. 17..
 */
public class Intenter {

    public static void call119(Context context) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "112"));
        context.startActivity(intent);
    }

    public static boolean sendEmerg(Context context) {
        ArrayList<String> nums = new ArrayList<String>();
        String[] strNums = Pref.init(context).getString(Pref.EMERGENCY_NUM, "").split("/");

        for (int i = 0 ; i < strNums.length ; i++) {
            if (!strNums[i].isEmpty()) {
                nums.add(strNums[i]);
            }
        }

        String numbersStr = TextUtils.join(",", nums);

        Uri uri = Uri.parse("sms:" + numbersStr);

        Intent intent = new Intent();
        intent.setData(uri);
        intent.putExtra(Intent.EXTRA_TEXT, Pref.init(context).getString(Pref.MSG, ""));
        intent.putExtra("sms_body", Pref.init(context).getString(Pref.MSG, ""));
        intent.putExtra("address", numbersStr);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setAction(Intent.ACTION_SENDTO);
            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(context);
            if(defaultSmsPackageName != null) {
                intent.setPackage(defaultSmsPackageName);
            }
        } else {
            intent.setAction(Intent.ACTION_VIEW);
            intent.setType("vnd.android-dir/mms-sms");
        }

        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
