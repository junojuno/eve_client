package com.cdp.eve.etc;

import android.os.Handler;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;


/**
 * Created by junojuno on 2015. 8. 19..
 */
public class TextCounter {
    public static void animateTextView(int initialValue, final int finalValue, final TextView textview) {
        if (finalValue < 20) {
            textview.setText(finalValue + "");
            return;
        }

        DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator(0.8f);

        int start = Math.min(initialValue, finalValue);
        int end = Math.max(initialValue, finalValue);
        int difference = Math.abs(finalValue - initialValue);

        Handler handler = new Handler();
        int time = 0;
        int tick = 0;

        for (int count = start; count <= end; count += (finalValue / 20), tick++ ) {
            time = Math.round(decelerateInterpolator.getInterpolation((((float) count) / difference)) * 40) * tick;

            final int finalCount = ((initialValue > finalValue) ? initialValue - count : count);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    textview.setText(finalCount + "");
                }
            }, time);
        }

        time += 150;

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                textview.setText(finalValue + "");
            }
        }, time);
    }
}
