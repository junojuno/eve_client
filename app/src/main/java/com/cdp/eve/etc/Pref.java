package com.cdp.eve.etc;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by junojuno on 2015. 7. 28..
 */
public class Pref {
    private static final String PREF_NAME = "eve";
    private static SharedPreferences pref;
    public static String TOKEN = "token";
    public static String ADDRESS = "address";
    public static String NAME = "name";
    public static String CITY = "city";
    public static String PICTURE = "picture";
    public static String EMERGENCY_NUM = "emergency_num";
    public static String MSG = "msg";
    public static String GCM = "gcm";

    public static String LATEST_LEVEL = "latest_level";
    public static String LATEST_CNT_ALARMS = "latest_cnt_alarms";

    public static String GCM_ON = "gcm_onoff";

    public static SharedPreferences init (Context context) {
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref;
    }

    public static SharedPreferences getPref() {
        return pref;
    }
}
