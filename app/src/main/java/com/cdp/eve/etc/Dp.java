package com.cdp.eve.etc;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

/**
 * Created by junojuno on 2015. 8. 16..
 */
public class Dp {
    public static float toPx(Context context, float dp) {
        Resources r = context.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return px;
    }
}
