package com.cdp.eve.etc;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by junojuno on 2015. 8. 12..
 */
public class TypefaceManager {

    public static String TYPEFACE_REGULAR = "regular.otf";
    public static String TYPEFACE_SEMIBOLD = "semibold.otf";
    public static String TYPEFACE_LIGHT = "light.otf";

    private static TypefaceManager instance;
    private final Context context;

    private Typeface tfLight;
    private Typeface tfRegular;
    private Typeface tfSemiBold;

    public static TypefaceManager getInstance(Context context) {
        if (instance == null) {
            instance = new TypefaceManager(context);
        }

        return instance;
    }

    private TypefaceManager(Context context) {
        this.context = context;
        this.tfLight = Typeface.createFromAsset(context.getAssets(), TYPEFACE_LIGHT);
        this.tfRegular = Typeface.createFromAsset(context.getAssets(), TYPEFACE_REGULAR);
        this.tfSemiBold = Typeface.createFromAsset(context.getAssets(), TYPEFACE_SEMIBOLD);
    }

    public Typeface getTypeface(String name) {
        if (name.equals(TYPEFACE_LIGHT)) {
            return tfLight;
        } else if (name.equals(TYPEFACE_SEMIBOLD)) {
            return tfSemiBold;
        } else if (name.equals(TYPEFACE_REGULAR)) {
            return tfRegular;
        }
        return tfRegular;
    }
}
