package com.cdp.eve.etc;

import android.content.Context;

import com.cdp.eve.R;

import java.util.Random;

/**
 * Created by junojuno on 2015. 8. 17..
 */
public class Menter {

    private final Context context;
    private static Menter instance;

    public static Menter getInstance(Context context) {
        if (instance == null) {
            instance = new Menter(context);
        }

        return instance;
    }

    private Menter(Context context) {
        this.context = context;
    }

    public String getMentByRand(int level) {
        String[] ments;

        switch (level) {
            case 1:
                ments = context.getResources().getStringArray(R.array.eve_ment_lv1);
                break;
            case 2:
                ments = context.getResources().getStringArray(R.array.eve_ment_lv2);
                break;
            case 3:
                ments = context.getResources().getStringArray(R.array.eve_ment_lv3);
                break;
            case 4:
                ments = context.getResources().getStringArray(R.array.eve_ment_lv4);
                break;
            case 5:
                ments = context.getResources().getStringArray(R.array.eve_ment_lv5);
                break;
            default:
                ments = context.getResources().getStringArray(R.array.eve_ment_lv1);
                break;
        }

        int ranNum = new Random().nextInt(ments.length);
        return ments[ranNum];
    }

}
