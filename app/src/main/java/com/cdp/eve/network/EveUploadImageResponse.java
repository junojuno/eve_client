package com.cdp.eve.network;

import org.json.JSONObject;

public interface EveUploadImageResponse {

    /**
     * 이미지 업로드(Multipart 형식)가 성공적인 경우에 콜백될 함수(옵저버패턴)
     * @param res 리스폰스 JSON 객체
     */
    public abstract void onImageUploadedResponse(JSONObject res);
}