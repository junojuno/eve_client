package com.cdp.eve.network;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.RequestQueue;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2015-06-28.
 */
public class API {
    public static class Caller {

        private static RequestQueue reqQ;
        public static void init (RequestQueue reqQ) {
            Caller.reqQ = reqQ;
        }

        public enum List {
            GET_TEST_USER, UPDATE_GCM, GET_WEEKLY_REPORT,
            IGNORE_ALARM, UPDATE_USER_INFO, IMAGE_UPLOAD;
        };

        public static final String URL = "http://1.255.57.224:10080/";
        public static final String GET_TEST_USER = "user/get/test";
        public static final String UPDATE_GCM = "user/update/gcm";
        public static final String GET_WEEKLY_REPORT = "report/get/weekly";
        public static final String IGNORE_ALARM = "alarm/ignore";
        public static final String UPDATE_USER_INFO = "user/update/info";
        public static final String IMAGE_UPLOAD = "upload/img";

        public static void getTestUser(Context context, EveResponse response) {
            String url = URL + GET_TEST_USER;
            reqQ.add(new EveRequest(context, List.GET_TEST_USER, url, response));
        }

        public static void updateGcm(Context context, String gcm, EveResponse response) {
            String url = URL + UPDATE_GCM;
            Map<String, String> params = new HashMap<String, String>();
            params.put("gcm", gcm);
            reqQ.add(new EveRequest(context, List.UPDATE_GCM, url, params, response));
        }

        public static void getWeeklyReport(Context context, EveResponse response) {
            String url = URL + GET_WEEKLY_REPORT;
            reqQ.add(new EveRequest(context, List.GET_WEEKLY_REPORT, url, response));
        }

        public static void ignoreAlarm(Context context, int alarmIdx, EveResponse response) {
            String url = URL + IGNORE_ALARM;
            Map<String, String> params = new HashMap<String, String>();
            params.put("alarm_idx", alarmIdx + "");

            reqQ.add(new EveRequest(context, List.IGNORE_ALARM, url, params, response));
        }

        public static void updateUserInfo(Context context, String name, String emerNum,
                                          String msg, String address,
                                          EveResponse response) {
            String url = URL + UPDATE_USER_INFO;

            Map<String, String> params = new HashMap<String, String>();
            params.put("name", name);
            params.put("emergency_num", emerNum);
            params.put("msg", msg);
            params.put("address", address);

            reqQ.add(new EveRequest(context, List.UPDATE_USER_INFO, url, params, response));
        }

        public static void updateUserInfo(Context context, String name, String emerNum,
                                          String msg, String address,
                                          int imgIdx, EveResponse response) {
            String url = URL + UPDATE_USER_INFO;

            Map<String, String> params = new HashMap<String, String>();
            params.put("name", name);
            params.put("emergency_num", emerNum);
            params.put("msg", msg);
            params.put("address", address);
            params.put("img_idx", imgIdx + "");

            reqQ.add(new EveRequest(context, List.UPDATE_USER_INFO, url, params, response));
        }

        public static void uploadImage(Context context, String imgName, Bitmap bmp, EveUploadImageResponse response) {
            String url = URL + IMAGE_UPLOAD;
            new EveUploadImageAsyncTask(context, url, bmp, imgName, response).execute();
        }

    }
}
