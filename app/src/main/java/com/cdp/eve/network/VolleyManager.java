package com.cdp.eve.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by jangjunho on 2014. 8. 11..
 */
public class VolleyManager {
    private static VolleyManager instance = null;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private VolleyManager(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mRequestQueue,new ImageLoader.ImageCache() {
            private LruCache<String, Bitmap> mCache;

            @Override
            public Bitmap getBitmap(String url) { return mCache.get(url); }

            @Override
            public void putBitmap(String url, Bitmap bitmap) { mCache.put(url,bitmap);}
        });
    }

    public static VolleyManager getInstance(Context context) {
        if (instance == null) instance = new VolleyManager(context);
        return instance;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public ImageLoader getmImageLoader() {
        return mImageLoader;
    }
}