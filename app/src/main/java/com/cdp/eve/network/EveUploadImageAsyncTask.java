package com.cdp.eve.network;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.cdp.eve.etc.Pref;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by jangjunho on 2014. 12. 7..
 */
public class EveUploadImageAsyncTask extends AsyncTask<Void, Void, JSONObject> {

    private final Context context;
    private final Bitmap img;
    private final String imgName;
    private EveUploadImageResponse mListener;

    String reqUrl;
    HttpClient httpclient;
    HttpPost httppost;

    public EveUploadImageAsyncTask(Context context, String url, Bitmap img, String imgName, EveUploadImageResponse listener) {
        this.mListener = listener;
        this.img = img;
        this.reqUrl = url;
        this.context = context;
        this.imgName = imgName;
    }

    /**
     * 이미지 업로드 하기 전 작업. 만약 토큰이 존재한다면 토큰을 싣어 보냄
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        httpclient = new DefaultHttpClient();
        httppost = new HttpPost(reqUrl);
    }

    /**
     * 실제 업로드하는 파
     * @param voids 없음
     * @return 업로드가 성공적인 경우 리스폰스를 JSON객체로 변환하여 리턴
     */
    @Override
    protected JSONObject doInBackground(Void... voids) {
        try {
            MultipartEntity entity = new MultipartEntity();
            SharedPreferences pref = Pref.getPref();

            String token = pref.getString(Pref.TOKEN, "");
            if (token != null) {
                httppost.setHeader("token", token);
            }
            entity.addPart("img", new ByteArrayBody(bmpToByteArr(img), imgName));

            httppost.setEntity(entity);
            HttpResponse responsePOST = httpclient.execute(httppost);
            HttpEntity resEntity = responsePOST.getEntity();
            String jsonString = EntityUtils.toString(resEntity);
            Log.e("jsonString", jsonString + " ISSS");

            return new JSONObject(jsonString);

        } catch (Exception e) {
            e.printStackTrace();

        }

        return null;

    }

    /**
     * 해당 리스폰스가 성공적인 경우에 UploadImageResponse에 onResponse 함수를 콜백
     * @param jsonObject doInBackground에서 전달받은 JSON 객체
     */
    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        try {
            if (jsonObject.getBoolean("success")) {
                Log.e("UploadImageResponse", jsonObject.toString());
                mListener.onImageUploadedResponse(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
        }

    }

    /**
     * 이미지를 전달 할 때 Bmp 객체를 ByteArray로 변환
     * @param bmp 타겟 Bitmap 파일
     * @return bmp를 ByteArray로 변환한 바이트 배열
     */
    public byte[] bmpToByteArr(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);

        return stream.toByteArray();

    }
}