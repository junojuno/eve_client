package com.cdp.eve.network;

import org.json.JSONObject;

public interface EveResponse {
    void onResponse(API.Caller.List apiCode, String url, JSONObject res);
}