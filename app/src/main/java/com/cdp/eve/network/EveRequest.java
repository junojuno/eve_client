package com.cdp.eve.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.cdp.eve.etc.Pref;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

class EveRequest extends Request<JSONObject> {

    Context context;
    API.Caller.List apiCode;
    Map<String, String> params;
    EveResponse response;

    public EveRequest(Context context, API.Caller.List apiCode, String url, Map<String, String> params, EveResponse response) {
        super(Method.POST, url, null);
        setParams(context, apiCode, url, params, response);
    }

    public EveRequest(Context context, API.Caller.List apiCode, String url, EveResponse response) {
        super(Method.GET, url, null);
        setParams(context, apiCode, url, null, response);
    }

    private void setParams(Context context, API.Caller.List apiCode, String url, Map<String, String> params, EveResponse response) {
        this.context = context;
        this.apiCode = apiCode;
        this.params = params;
        this.response = response;
        Log.e("request_url", url);
        if (params != null) {
            for (String key : params.keySet()) {
                Log.e("request_params", key + " = " + params.get(key));
            }
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        String token = Pref.getPref().getString(Pref.TOKEN, null);

        if (token != null) {
            Map<String, String> headers = new HashMap<String, String>();
            headers.put("token", token);
            return headers;
        }

        return super.getHeaders();
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        if (params != null) return params;
        else return super.getParams();
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            Log.e("response", jsonString);
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(JSONObject jsonObject) {
        response.onResponse(apiCode, getUrl(), jsonObject);
    }

}