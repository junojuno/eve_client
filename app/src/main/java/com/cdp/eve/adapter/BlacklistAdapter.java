package com.cdp.eve.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.cdp.eve.model.Alarm;
import com.cdp.eve.view.BlacklistView;
import com.cdp.eve.view.BlacklistView_;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by junojuno on 2015. 8. 7..
 */
public class BlacklistAdapter extends BaseAdapter {

    private final Context context;
    private final List<Alarm> alarms;

    public BlacklistAdapter(Context context, List<Alarm> alarms) {
        this.context = context;
        this.alarms = alarms;
    }

    @Override
    public int getCount() {
        return alarms.size();
    }

    @Override
    public Alarm getItem(int i) {
        return alarms.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        BlacklistView view;

        if (convertView == null) {
            view = BlacklistView_.build(context);
        } else {
            view = (BlacklistView) convertView;
        }

        view.bind(getItem(i));
        return view;
    }
}
