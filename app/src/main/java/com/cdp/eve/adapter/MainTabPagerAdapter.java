package com.cdp.eve.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.cdp.eve.fragment.AnalysisFragment_;
import com.cdp.eve.fragment.BaseFragment;
import com.cdp.eve.fragment.PhotosFragment_;
import com.cdp.eve.fragment.TimelineFragment;
import com.cdp.eve.fragment.TimelineFragment_;

/**
 * Created by Administrator on 2015-07-02.
 */
public class MainTabPagerAdapter extends FragmentStatePagerAdapter {

    BaseFragment fragments[];

    private static final String[] TITLE_PAGES = {"Analysis", "Timeline", " Photos "};

    public MainTabPagerAdapter(FragmentManager fm) {
        super(fm);
        fragments = new BaseFragment[3];
    }

    public void startMent() {
        Fragment frag = fragments[1];

        if (frag != null) {
            ((TimelineFragment) frag).startMent();
        }
    }

    public void stopMent() {
        Fragment frag = fragments[1];

        if (frag != null) {
            ((TimelineFragment) frag).stopMent();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLE_PAGES[position];
    }

    @Override
    public Fragment getItem(int position) {
        Log.e("poss", position + "");
//        if (position == 0) {
//            return AnalysisFragment_.builder().build();
//        }
        if (fragments[position] == null) {
            switch (position) {
                case 0:
                    Log.e("Paa","BO");
                    fragments[0] = AnalysisFragment_.builder().build();
                    break;
                case 1:
                    fragments[1] = TimelineFragment_.builder().build();
                    break;
                case 2:
                    fragments[2] = PhotosFragment_.builder().build();
                    break;
//                default:
//                    fragments[0] = AnalysisFragment_.builder().build();
            }
        }

        return fragments[position];
    }

    @Override
    public int getCount() {
        return TITLE_PAGES.length;
    }

    public void invalidate(int i) {
        if (fragments[i] != null) {
            fragments[i].invalidate();
        }
    }
}
