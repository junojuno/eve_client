package com.cdp.eve.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.cdp.eve.model.Statis;
import com.cdp.eve.view.StatisView;
import com.cdp.eve.view.StatisView_;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015-07-13.
 */
public class StatisAdapter extends BaseAdapter {
    private ArrayList<Statis> items;
    private Context context;

    public StatisAdapter(Context context, ArrayList<Statis> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Statis getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        StatisView itemView;

        if (convertView == null) {
            itemView = StatisView_.build(context);
        } else {
            itemView = (StatisView) convertView;
        }

        itemView.bind(getItem(position));

        return itemView;
    }
}
