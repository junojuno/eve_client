package com.cdp.eve.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.cdp.eve.etc.Dp;
import com.cdp.eve.model.Alarm;
import com.cdp.eve.view.BlacklistView;
import com.cdp.eve.view.BlacklistView_;
import com.squareup.picasso.Picasso;

import java.util.List;

import it.sephiroth.android.library.widget.HListView;

/**
 * Created by junojuno on 2015. 8. 7..
 */
public class PhotoDetailListAdapter extends BaseAdapter {

    private final Context context;
    private final List<Alarm> alarms;

    public PhotoDetailListAdapter(Context context, List<Alarm> alarms) {
        this.context = context;
        this.alarms = alarms;
    }

    @Override
    public int getCount() {
        return alarms.size();
    }

    @Override
    public Alarm getItem(int i) {
        return alarms.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ImageView view;

        if (convertView == null) {
            view = new ImageView(context);
            ViewGroup.LayoutParams params = new HListView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.width = (int) Dp.toPx(context, 65.25f);
            params.height = (int) Dp.toPx(context, 65.25f);
            view.setLayoutParams(params);
        } else {
            view = (ImageView) convertView;
        }

        Picasso.with(context).load(getItem(i).getImg()).into(view);
        return view;
    }
}
