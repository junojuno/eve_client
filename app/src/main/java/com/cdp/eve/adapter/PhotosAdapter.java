package com.cdp.eve.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.cdp.eve.model.Alarm;
import com.cdp.eve.view.EvePhotoView;
import com.cdp.eve.view.EvePhotoView_;
import com.cdp.eve.widget.EvePhotoImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by junojuno on 2015. 8. 8..
 */
public class PhotosAdapter extends BaseAdapter {

    private final Context context;
    private final List<Alarm> alarms;

    public PhotosAdapter(Context context, List<Alarm> alarms) {
        this.context = context;
        this.alarms = alarms;
    }

    @Override
    public int getCount() {
        return alarms.size();
    }

    @Override
    public Alarm getItem(int i) {
        return alarms.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
//        EvePhotoView view = null;
//
//        if (convertView == null) {
//            view = EvePhotoView_.build(context);
//        } else {
//            view = (EvePhotoView) convertView;
//        }
//
//        view.bind(context, alarms.get(i).getImg());
        EvePhotoImageView view;

        if (convertView == null) {
            view = new EvePhotoImageView(context);
        } else {
            view = (EvePhotoImageView) convertView;
        }

        view.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Picasso.with(context).load(alarms.get(i).getImg()).into(view);

        return view;
    }
}
