package com.cdp.eve.adapter;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cdp.eve.fragment.guide.GuideFinishFragment_;
import com.cdp.eve.fragment.guide.GuideProfileFragment_;
import com.cdp.eve.fragment.guide.GuideSyncFragment_;
import com.cdp.eve.fragment.guide.GuideStartFragment_;


public class GuidePagerAdapter extends FragmentStatePagerAdapter {

    private static final int CNT_PAGE = 4;

    public GuidePagerAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return GuideStartFragment_.builder().build();
            case 1:
                return GuideSyncFragment_.builder().build();
            case 2:
                return GuideProfileFragment_.builder().build();
            case 3:
                return GuideFinishFragment_.builder().build();
            default:
                return GuideStartFragment_.builder().build();
        }
    }

    @Override
    public int getCount() {
        return CNT_PAGE;
    }
}
