package com.cdp.eve.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cdp.eve.R;
import com.cdp.eve.etc.Pref;
import com.cdp.eve.etc.TypefaceManager;
import com.cdp.eve.network.API;
import com.cdp.eve.network.EveResponse;
import com.cdp.eve.network.EveUploadImageResponse;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

@EActivity(R.layout.activity_profile)
public class ProfileActivity extends BaseActivity implements EveUploadImageResponse, EveResponse {

    private static final int RESULT_LOAD_IMAGE = 10010;
    @ViewById(R.id.civProfilePicture)
    CircleImageView civPicture;

    @ViewById(R.id.tvProfileName)
    TextView tvName;

    @ViewById(R.id.tvProfileAddress)
    TextView tvAddress;

    @ViewById(R.id.tvProfileEmerNum)
    TextView tvEmerNum;

    @ViewById(R.id.tvProfileMsg)
    TextView tvMsg;

    @ViewById(R.id.etProfileName)
    EditText etName;

    @ViewById(R.id.etProfileAddress)
    EditText etAddress;

    @ViewById(R.id.etProfileMsg)
    EditText etMsg;

    @ViewById(R.id.btnProfileComplete)
    Button btnComplete;

    @ViewById(R.id.btnProfileAddNum)
    ImageButton btnAddNum;

    @ViewById(R.id.llProfileNums)
    LinearLayout llNums;

    String namePicture;
    Bitmap bmpPicture;

    int imgIdx;

    @Click(R.id.btnProfileAddNum)
    void addNums() {
        llNums.addView(getNumsView(""));
        llNums.addView(getLine());

    }

    @Click(R.id.btnProfileBack)
    void cancelUpdate() {
        finish();
    }

    @Click(R.id.btnProfileComplete)
    void completeUpdate() {
        if (bmpPicture != null) {
            API.Caller.uploadImage(this, namePicture, bmpPicture, this);
        } else {
            update();
        }
    }

    private void update() {
        String name = etName.getText().toString();
        String address = etAddress.getText().toString();

        String hp = "";
        for (int i = 0 ; i < llNums.getChildCount() ; i++) {
            View view = llNums.getChildAt(i);
            if (view instanceof EditText) {
                EditText child = (EditText)view;
                if (!child.getText().toString().isEmpty()) {
                    hp += child.getText().toString() + "/";
                }
            }
        }

        String msg = etMsg.getText().toString();

        if (imgIdx == 0) {
            API.Caller.updateUserInfo(this, name, hp, msg, address, this);
        } else {
            API.Caller.updateUserInfo(this, name, hp, msg, address, imgIdx, this);
        }
    }

    @Click(R.id.civProfilePicture)
    void loadPicture() {
        civPicture.setClickable(false);
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        civPicture.setClickable(true);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            String[] slices = picturePath.split("/");

            BitmapFactory.Options options = new BitmapFactory.Options();

            bmpPicture = BitmapFactory.decodeFile(picturePath, options);
            if (bmpPicture.getWidth() >= 480) {
                bmpPicture = Bitmap.createScaledBitmap(bmpPicture, bmpPicture.getWidth() / 2, bmpPicture.getHeight() / 2, true);
            }

            namePicture = slices[slices.length - 1];
            civPicture.setImageBitmap(bmpPicture);

        }
    }

    @AfterViews
    void init () {
        imgIdx = 0;
        bmpPicture = null;
        namePicture = null;

        Typeface regular = TypefaceManager.getInstance(this).getTypeface(TypefaceManager.TYPEFACE_REGULAR);
        tvName.setTypeface(regular);
        tvAddress.setTypeface(regular);
        tvEmerNum.setTypeface(regular);
        tvMsg.setTypeface(regular);
        btnComplete.setTypeface(regular);

        etName.setTypeface(regular);
        etAddress.setTypeface(regular);
        etMsg.setTypeface(regular);

        pref = Pref.getPref();

        String pic = pref.getString(Pref.PICTURE, "");
        String name = pref.getString(Pref.NAME, "");
        String address = pref.getString(Pref.ADDRESS, "");
        String msg = pref.getString(Pref.MSG, "");
        String hp = pref.getString(Pref.EMERGENCY_NUM, "");

        Picasso.with(this).load(pic).into(civPicture);
        etName.setText(name);
        etAddress.setText(address);
        etMsg.setText(msg);
        initNums(hp.split("/"));
    }

    private void initNums(String[] nums) {
        for (int i = 0 ; i < nums.length ; i++) {
            if (!nums[i].isEmpty()) {
                llNums.addView(getNumsView(nums[i]));
                llNums.addView(getLine());
            }
        }
    }

    private EditText getNumsView(String num) {
        Typeface regular = TypefaceManager.getInstance(this).getTypeface(TypefaceManager.TYPEFACE_REGULAR);

        EditText etView = new EditText(this);
        etView.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        etView.setText(num);
        etView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        etView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        etView.setTextColor(getResources().getColor(R.color.all_three));
        etView.setTypeface(regular);
        return etView;
    }

    private View getLine() {
        View view = new View(this);
        view.setBackgroundColor(getResources().getColor(R.color.line_color));
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        param.height = 2;
        view.setLayoutParams(param);
        return view;
    }

    @Override
    public void onImageUploadedResponse(JSONObject res) {
        try {
            if (res.getBoolean("success")) {
                imgIdx = res.getJSONObject("data").getInt("idx");
                update();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResponse(API.Caller.List apiCode, String url, JSONObject res) {
        if (apiCode == API.Caller.List.UPDATE_USER_INFO) {
            try {
                if (res.getBoolean("success")) {
                    JSONObject data = res.getJSONObject("data");

                    SharedPreferences.Editor editor = Pref.getPref().edit();
                    editor.putString(Pref.TOKEN, data.getString("token"));
                    editor.putString(Pref.PICTURE, data.getString("picture"));
                    editor.putString(Pref.ADDRESS, data.getString("address"));
                    editor.putString(Pref.NAME, data.getString("name"));
                    editor.putString(Pref.EMERGENCY_NUM, data.getString("emergency_num"));
                    editor.putString(Pref.MSG, data.getString("msg"));
                    editor.commit();

                    setResult(RESULT_OK);
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
