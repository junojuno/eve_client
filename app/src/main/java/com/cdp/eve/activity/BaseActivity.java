package com.cdp.eve.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.cdp.eve.etc.Constant;


public class BaseActivity extends FragmentActivity implements Constant{
    protected SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = getSharedPreferences(NAME_PREF, MODE_PRIVATE);
    }
}
