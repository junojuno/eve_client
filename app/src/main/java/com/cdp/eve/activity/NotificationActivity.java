package com.cdp.eve.activity;

import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.cdp.eve.R;
import com.cdp.eve.etc.Intenter;
import com.cdp.eve.etc.TypefaceManager;
import com.cdp.eve.network.API;
import com.cdp.eve.network.EveResponse;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

@EActivity(R.layout.activity_notification)
public class NotificationActivity extends BaseActivity implements EveResponse {

    @ViewById(R.id.rlNotiConfirm)
    RelativeLayout rlConfirm;

    @ViewById(R.id.ivNotiImg)
    ImageView ivNotiImg;

    @ViewById(R.id.llNotiConfirmNo)
    LinearLayout llNo;

    @ViewById(R.id.llNotiConfirmYes)
    LinearLayout llYes;

    @ViewById(R.id.tvNotiConfirmTitle)
    TextView tvConfirm;

    @ViewById(R.id.tvNotiConfirmNo)
    TextView tvNo;

    @ViewById(R.id.tvNotiConfirmYes)
    TextView tvYes;

    @ViewById(R.id.ivNotiLevelBg)
    ImageView ivBg;

    @Extra("imgUrl")
    String imgUrl;

    @Extra("level")
    int level;

    @Extra("imgIdx")
    int alarmIdx;

    @Click(R.id.llNoti119)
    void call119() {
        Intenter.call119(this);
    }

    @Click(R.id.llNotiEmerg)
    void sendSms() {
        Intenter.sendEmerg(this);
    }

    @Click({R.id.llNotiConfirmNo, R.id.llNotiConfirmYes})
    void confirm(View view) {
        switch (view.getId()) {
            case R.id.llNotiConfirmNo:
                finish();
                break;
            case R.id.llNotiConfirmYes:
                API.Caller.init(Volley.newRequestQueue(this));
                API.Caller.ignoreAlarm(this, alarmIdx, this);
                break;
        }
    }

    @AfterViews
    void init() {
        int imgRes = R.drawable.bg_noti_lv2;

        switch (level) {
            case 1:
                imgRes = R.drawable.bg_noti_lv1;
                break;
            case 2:
                imgRes = R.drawable.bg_noti_lv2;
                break;
            case 3:
                imgRes = R.drawable.bg_noti_lv3;
                break;
            case 4:
                imgRes = R.drawable.bg_noti_lv4;
                break;
            case 5:
                imgRes = R.drawable.bg_noti_lv5;
                break;
        }

        ivBg.setBackgroundResource(imgRes);
        ivBg.setAlpha(0.7f);

        Typeface light = TypefaceManager.getInstance(this).getTypeface(TypefaceManager.TYPEFACE_LIGHT);

        tvConfirm.setTypeface(light);
        tvYes.setTypeface(light);
        tvNo.setTypeface(light);
        Picasso.with(this).load(imgUrl).fit().into(ivNotiImg);
    }

    @Override
    public void onResponse(API.Caller.List apiCode, String url, JSONObject res) {
        if (apiCode == API.Caller.List.IGNORE_ALARM) {
            try {
                if (res.getBoolean("success")) {
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
