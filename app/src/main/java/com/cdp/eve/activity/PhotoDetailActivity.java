package com.cdp.eve.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.Telephony;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cdp.eve.R;
import com.cdp.eve.adapter.PhotoDetailListAdapter;
import com.cdp.eve.etc.Intenter;
import com.cdp.eve.etc.Pref;
import com.cdp.eve.etc.TypefaceManager;
import com.cdp.eve.fragment.BaseFragment;
import com.cdp.eve.model.Alarm;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import it.sephiroth.android.library.widget.AdapterView;
import it.sephiroth.android.library.widget.HListView;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifTextView;

@EActivity(R.layout.activity_photo_detail)
public class PhotoDetailActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    @ViewById(R.id.rlPhotoDetail)
    RelativeLayout rlDetail;

    @ViewById(R.id.llPhotoDetailHeader)
    LinearLayout llHeader;

    @ViewById(R.id.ivPhotoDetail)
    ImageView ivDetail;

    @ViewById(R.id.hlvPhotoDetailList)
    HListView hlvList;

    @ViewById(R.id.tvPhotoDetailTime)
    TextView tvTime;

    @ViewById(R.id.tvPhotoDetailDuration)
    TextView tvDur;

    @Extra("alarm")
    Alarm alarm;

    @Extra("alarms")
    List<Alarm> alarms;

    @ViewById(R.id.rlPhotoDetailBot)
    RelativeLayout rlBot;

    @ViewById(R.id.gtvPhotoDetailVibration)
    GifTextView gtvVibration;

    @ViewById(R.id.ivPhotoDetail119)
    ImageView iv119;

    @ViewById(R.id.ivPhotoDetailEmer)
    ImageView ivEmer;

    @ViewById(R.id.ivPhotoDetailDel)
    ImageView ivDel;

    PhotoDetailListAdapter adapter;

    @Click(R.id.ivPhotoDetailBack)
    void back() {
        finish();
    }

    @Click(R.id.ivPhotoDetail119)
    void callTo119() {
        Intenter.call119(this);
    }

    @Click(R.id.ivPhotoDetailEmer)
    void sendEmer() {
        Intenter.sendEmerg(this);
    }

    @Click(R.id.ivPhotoDetailDel)
    void delAlarm() {

    }
    @Click(R.id.rlPhotoDetail)
    void backgroundClick() {
        if (llHeader.getVisibility() == View.GONE) {
            llHeader.setVisibility(View.VISIBLE);
            hlvList.setVisibility(View.VISIBLE);
            rlBot.setVisibility(View.GONE);
        } else {
            llHeader.setVisibility(View.GONE);
            hlvList.setVisibility(View.GONE);
            rlBot.setVisibility(View.VISIBLE);
        }
    }

    @AfterViews
    void init () {
        Typeface light = TypefaceManager.getInstance(this).getTypeface(TypefaceManager.TYPEFACE_LIGHT);
        tvDur.setTypeface(light);
        tvTime.setTypeface(light);

        adapter = new PhotoDetailListAdapter(this, alarms);
        hlvList.setAdapter(adapter);
        hlvList.setOnItemClickListener(this);
        setAlarmView(alarm);
        setVibrate(alarm);
    }

    void setAlarmView(Alarm alarm) {
        Picasso.with(this).load(alarm.getImg()).into(ivDetail);
        tvTime.setText(String.format("Duration Time : %dmin", alarm.getDiff()));
        tvDur.setText(new SimpleDateFormat("yyyy. M.dd H:mm").format(alarm.getEndTime()));

        int res = 0;
        switch (alarm.getLevel()) {
            case 1:
                res = R.drawable.bg_photo_detail_bot_lv1;
                break;
            case 2:
                res = R.drawable.bg_photo_detail_bot_lv2;
                break;
            case 3:
                res = R.drawable.bg_photo_detail_bot_lv3;
                break;
            case 4:
                res = R.drawable.bg_photo_detail_bot_lv4;
                break;
            case 5:
                res = R.drawable.bg_photo_detail_bot_lv5;
                break;
        }

        rlBot.setBackgroundResource(res);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
        Alarm item = alarms.get(pos);
        setAlarmView(item);
        setVibrate(item);
    }

    private void setVibrate(Alarm item) {
        if (item.isVibrate() == 1) {
            try {
                gtvVibration.setVisibility(View.VISIBLE);
                GifDrawable gifFromAssets = null;
                gifFromAssets = new GifDrawable(getAssets(), "vibration.gif");
                gtvVibration.setBackground(gifFromAssets);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            gtvVibration.setVisibility(View.GONE);
        }
    }
}
