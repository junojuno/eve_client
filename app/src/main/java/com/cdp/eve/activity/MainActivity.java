package com.cdp.eve.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.VideoView;

import com.andexert.library.RippleView;
import com.cdp.eve.R;
import com.cdp.eve.adapter.MainTabPagerAdapter;
import com.cdp.eve.etc.Pref;
import com.cdp.eve.etc.TypefaceManager;
import com.cdp.eve.model.Alarm;
import com.cdp.eve.network.API;
import com.cdp.eve.network.EveResponse;
import com.cdp.eve.widget.LazyViewPager;
import com.squareup.picasso.Picasso;

import net.yanzm.mth.MaterialTabHost;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import pl.droidsonroids.gif.GifTextView;

@EActivity(R.layout.activity_main)
public class MainActivity extends ActionBarActivity implements MaterialTabHost.OnTabChangeListener {

    private static final int REQ_PROFILE_UPDATE = 100;

    @ViewById(R.id.thMainTab)
    MaterialTabHost tabHost;

    @ViewById(R.id.vpMainTab)
    LazyViewPager pager;

    @ViewById(R.id.tbMain)
    Toolbar tbMain;

    @ViewById(R.id.dlMainMenu)
    DrawerLayout dlMenu;

    @ViewById(R.id.civMenuPicture)
    CircleImageView civPicture;

    @ViewById(R.id.tvMenuHi)
    TextView tvHi;

    @ViewById(R.id.tvMenuProfile)
    TextView tvProfifle;

    @ViewById(R.id.tvMenuNoti)
    TextView tvNoti;

    @ViewById(R.id.tvMenuBattery)
    TextView tvBattery;

    @ViewById(R.id.tvMenuBatteryPercent)
    TextView tvBatteryPer;

    @ViewById(R.id.tvMenuWifi)
    TextView tvWifi;

    @ViewById(R.id.tvMenuWifiName)
    TextView tvWifiName;

    @ViewById(R.id.tbtnMenuNoti)
    ToggleButton tbtnNoti;

    @ViewById(R.id.rvMenuProfile)
    RippleView rvProfile;

    @ViewById(R.id.ivMenuProfileBg)
    ImageView ivProfifleBg;

    MainTabPagerAdapter pagerAdapter;
    private ActionBarDrawerToggle dtToggle;

    int prevSelectedTab;
    private Menu actionbarMenu;
    private MenuItem filterItem;

    @Extra("alarms")
    ArrayList<Alarm> alarms;

    public ArrayList<Alarm> getAlarms() {
        return alarms;
    }

    @CheckedChange(R.id.tbtnMenuNoti)
    void notiSetToggled(CompoundButton button, boolean isChecked) {
        Pref.getPref().edit().putBoolean(Pref.GCM_ON, isChecked).commit();
    }

//    @Click(R.id.llMenuProfile)
    void clickProfile() {
        dlMenu.closeDrawers();
        ProfileActivity_.intent(this).startForResult(REQ_PROFILE_UPDATE);
    }

    public String getWifiName(Context context) {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (manager.isWifiEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    return wifiInfo.getSSID();
                }
            }
        }
        return null;
    }

    @AfterViews
    void init() {
        prevSelectedTab = 0;
        pager.setOffscreenPageLimit(3);
        setSupportActionBar(tbMain);
        getSupportActionBar().setTitle("");
        initProfile();
        initTabhost();
        initPager();

        tvHi.setTypeface(TypefaceManager.getInstance(this).getTypeface(TypefaceManager.TYPEFACE_LIGHT));

        Typeface regular = TypefaceManager.getInstance(this).getTypeface(TypefaceManager.TYPEFACE_REGULAR);

        tvProfifle.setTypeface(regular);
        tvNoti.setTypeface(regular);
        tvBattery.setTypeface(regular);
        tvWifi.setTypeface(regular);

        tvBatteryPer.setTypeface(regular);
        tvWifiName.setTypeface(regular);

        rvProfile.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                clickProfile();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu);
    }

    private void initProfile() {
        SharedPreferences pref = Pref.getPref();
        String name = pref.getString(Pref.NAME, "");
        String pic = pref.getString(Pref.PICTURE, "");
//        int level = pref.getInt(Pref.LATEST_LEVEL, 2);
        boolean isNotiOn = pref.getBoolean(Pref.GCM_ON, true);

        int bg = 0;

        if (alarms.size() <= 15) {
            bg = R.drawable.bg_menu_lv1;
        } else if (alarms.size() <= 30) {
            bg = R.drawable.bg_menu_lv2;
        } else if (alarms.size() <= 45) {
            bg = R.drawable.bg_menu_lv3;
        } else if (alarms.size() <= 60) {
            bg = R.drawable.bg_menu_lv4;
        } else {
            bg = R.drawable.bg_menu_lv5;
        }

        ivProfifleBg.setBackgroundResource(bg);
        tvWifiName.setText(getWifiName(this));
        tbtnNoti.setChecked(isNotiOn);
        Picasso.with(this).load(pic).resize(500, 500).centerInside().into(civPicture);
        tvHi.setText(String.format("Have a good evening, %s!", name));
    }

    void initPager() {
        pagerAdapter = new MainTabPagerAdapter(getSupportFragmentManager());
        for (int i = 0; i < pagerAdapter.getCount(); i++) {

            TextView tv = new TextView(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            params.weight = 1;
            tv.setGravity(Gravity.CENTER);
            tv.setLayoutParams(params);
            tv.setText(pagerAdapter.getPageTitle(i));

            setTextTabHost(TypefaceManager.getInstance(this).getTypeface(TypefaceManager.TYPEFACE_LIGHT)
                    , tv, 14, getResources().getColor(R.color.tab_not_selected));

            tabHost.addTab(tv);
        }

        pager.setAdapter(pagerAdapter);
        pager.setOnPageChangeListener(tabHost);
        pager.setCurrentItem(1);
    }

    void setTextTabHost(Typeface font, TextView tv, int sp, int color) {
        tv.setTypeface(font);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, sp);
        tv.setTextColor(color);
    }

    void initTabhost() {
        tabHost.setType(MaterialTabHost.Type.FullScreenWidth);
        tabHost.setOnTabChangeListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        actionbarMenu = menu;
        filterItem = actionbarMenu.findItem(R.id.action_filter);
        filterItem.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                dlMenu.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_PROFILE_UPDATE) {
            if (resultCode == RESULT_OK) {
                initProfile();
            }
        }
    }

    @Override
    public void onTabSelected(int i) {
//        pagerAdapter.invalidate(i);

//        if (i == 0) {
//            pagerAdapter.invalidate(i);
//        }

        if (i == 1) {
            pagerAdapter.startMent();
        } else {
            pagerAdapter.stopMent();
        }

        if (filterItem != null) {
            if (i == 2) {
                filterItem.setVisible(true);
            } else {
                filterItem.setVisible(false);
            }
        }

        TextView prevView = (TextView) tabHost.getTabWidget().getChildTabViewAt(prevSelectedTab);
        setTextTabHost(TypefaceManager.getInstance(this).getTypeface(TypefaceManager.TYPEFACE_LIGHT),
                prevView, 14, getResources().getColor(R.color.tab_not_selected));

        pager.setCurrentItem(i);
        tabHost.onPageSelected(i);

        TextView view = (TextView) tabHost.getTabWidget().getChildTabViewAt(i);
        setTextTabHost(TypefaceManager.getInstance(this).getTypeface(TypefaceManager.TYPEFACE_REGULAR),
                view, 16, getResources().getColor(R.color.tab_selected));

        prevSelectedTab = i;
    }
}
