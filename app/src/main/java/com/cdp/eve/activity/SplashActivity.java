package com.cdp.eve.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.android.volley.toolbox.Volley;
import com.cdp.eve.R;
import com.cdp.eve.etc.Pref;
import com.cdp.eve.model.Alarm;
import com.cdp.eve.network.API;
import com.cdp.eve.network.EveResponse;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends Activity implements EveResponse, Animation.AnimationListener {

    private static final String SENDER_ID = "1067311543721";

    private ArrayList<Alarm> alarms;


    @ViewById(R.id.ivSplash)
    ImageView ivSplash;

    @AfterViews
    void init () {
        final Animation animationFadeIn = AnimationUtils.loadAnimation(this, R.anim.fadein);
        animationFadeIn.setAnimationListener(this);
        ivSplash.startAnimation(animationFadeIn);
    }

    public static int getThemeAttributeDimensionSize(Context context, int attr)
    {
        TypedArray a = null;
        try{
            a = context.getTheme().obtainStyledAttributes(new int[] { attr });
            return a.getDimensionPixelSize(0, 0);
        }finally{
            if(a != null){
                a.recycle();
            }
        }
    }

    private void registerInBackground() {

        new AsyncTask<Void, Void, String>() {
            GoogleCloudMessaging gcm;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                gcm = GoogleCloudMessaging.getInstance(SplashActivity.this);
            }

            @Override
            protected String doInBackground(Void... params) {
                try {
                    return gcm.register(SENDER_ID);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (msg != null) {
                    API.Caller.updateGcm(SplashActivity.this, msg, SplashActivity.this);
                }
            }
        }.execute(null, null, null);
    }

    @Override
    public void onResponse(API.Caller.List apiCode, String url, JSONObject res) {
        if (apiCode == API.Caller.List.GET_TEST_USER) {
            try {
                if (res.getBoolean("success")) {
                    JSONObject data = res.getJSONObject("data");

                    SharedPreferences.Editor editor = Pref.getPref().edit();
                    editor.putString(Pref.TOKEN, data.getString("token"));
                    editor.putString(Pref.CITY, data.getString("city"));
                    editor.putString(Pref.PICTURE, data.getString("picture"));
                    editor.putString(Pref.ADDRESS, data.getString("address"));
                    editor.putString(Pref.NAME, data.getString("name"));
                    editor.putString(Pref.EMERGENCY_NUM, data.getString("emergency_num"));
                    editor.putString(Pref.MSG, data.getString("msg"));
                    editor.commit();

                    registerInBackground();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (apiCode == API.Caller.List.UPDATE_GCM) {
            try {
                if (res.getBoolean("success")) {
                    API.Caller.getWeeklyReport(this, this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (apiCode == API.Caller.List.GET_WEEKLY_REPORT) {
            try {
                if (res.getBoolean("success")) {
                    Log.e("?","1");
                    JSONArray jar = res.getJSONObject("data").getJSONArray("alarms");
                    alarms = new ArrayList<Alarm>();

                    Alarm alarm = null;
                    for (int i = 0 ; i < jar.length() ; i++) {
                        JSONObject job = jar.getJSONObject(i);

                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        alarm = new Alarm(
                                job.getInt("diff"),
                                job.getString("picture"),
                                job.getInt("level"),
                                format.parse(job.getString("start_time")),
                                format.parse(job.getString("end_time")),
                                job.getInt("is_vibrate")
                        );
                        alarms.add(alarm);
                    }
                    Log.e("?","2");


                    MainActivity_.intent(SplashActivity.this).alarms(alarms).start();
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public void onAnimationStart(Animation animation) {
        ivSplash.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        Pref.init(this);
        API.Caller.init(Volley.newRequestQueue(this));
        API.Caller.getTestUser(this, this);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
