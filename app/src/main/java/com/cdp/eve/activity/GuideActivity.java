package com.cdp.eve.activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.cdp.eve.R;
import com.cdp.eve.adapter.GuidePagerAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_guide)
public class GuideActivity extends BaseActivity {

    @ViewById(R.id.vpGuide)
    ViewPager vpGuide;

    GuidePagerAdapter pagerAdapter;

    @AfterViews
    void init() {
        pagerAdapter = new GuidePagerAdapter(getSupportFragmentManager());
        vpGuide.setAdapter(pagerAdapter);
    }

    public void next(View view) {
        int current = vpGuide.getCurrentItem();
        Log.e("cur", current + "");
        if (current < pagerAdapter.getCount() - 1) {
            vpGuide.setCurrentItem(current + 1);
        } else {
            MainActivity_.intent(this).start();
        }
    }
}
