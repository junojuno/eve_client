package com.cdp.eve.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by junojuno on 2015. 8. 8..
 */
public class EvePhotoImageView extends ImageView {
    public EvePhotoImageView(Context context) {
        super(context);
    }

    public EvePhotoImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EvePhotoImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()); //Snap to width
    }
}
