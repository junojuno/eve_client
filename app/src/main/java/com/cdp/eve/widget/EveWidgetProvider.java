package com.cdp.eve.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.RemoteViews;

import com.cdp.eve.R;
import com.cdp.eve.activity.SplashActivity;
import com.cdp.eve.activity.SplashActivity_;
import com.cdp.eve.etc.Pref;

/**
 * Created by junojuno on 2015. 7. 28..
 */
public class EveWidgetProvider extends AppWidgetProvider{

    private static final String WIDGET_CLICKED = "widgetClicked";

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (WIDGET_CLICKED.equals(intent.getAction())) {
            SplashActivity_.intent(context).flags(Intent.FLAG_ACTIVITY_NEW_TASK).start();
        } else if(intent.getAction().equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
            AppWidgetManager man = AppWidgetManager.getInstance(context);
            ComponentName thisAppWidget = new ComponentName(context.getPackageName(), EveWidgetProvider.class.getName());
            int[] appWidgetIds = man.getAppWidgetIds(thisAppWidget);

            onUpdate(context, man, appWidgetIds);
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, getClass()));


        SharedPreferences pref = Pref.init(context);

        int level = pref.getInt(Pref.LATEST_LEVEL, 2);
        int alarms = pref.getInt(Pref.LATEST_CNT_ALARMS, 0);

        RemoteViews remoteViews;
        ComponentName watchWidget;

        remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_eve);
        watchWidget = new ComponentName(context, EveWidgetProvider.class);

        remoteViews.setOnClickPendingIntent(R.id.rlWidget, getPendinIntent(context, WIDGET_CLICKED));
        remoteViews.setTextViewText(R.id.tvWidgetTotalCnt, alarms + "");

        int bg = 0;
        switch (level) {
            case 1:
                bg = R.drawable.bg_widget_lv1;
                break;
            case 2:
                bg = R.drawable.bg_widget_lv2;
                break;
            case 3:
                bg = R.drawable.bg_widget_lv3;
                break;
            case 4:
                bg = R.drawable.bg_widget_lv4;
                break;
            case 5:
                bg = R.drawable.bg_widget_lv5;
                break;
        }

        remoteViews.setImageViewResource(R.id.ivWidgetBg, bg);
        appWidgetManager.updateAppWidget(watchWidget, remoteViews);
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
    }

    public PendingIntent getPendinIntent(Context context, String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

}
