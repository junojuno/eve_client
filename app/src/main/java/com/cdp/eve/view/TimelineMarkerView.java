package com.cdp.eve.view;

import android.content.Context;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;

/**
 * Created by junojuno on 2015. 8. 5..
 */
public class TimelineMarkerView extends MarkerView {

    public TimelineMarkerView (Context context, int layoutResource) {
        super(context, layoutResource);
//        // this markerview only displays a textview
//        tvContent = (TextView) findViewById(R.id.tvContent);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, int dataSetIndex) {
//        tvContent.setText("" + e.getVal()); // set the entry-value as the display text
    }

    @Override
    public int getXOffset() {
//        // this will center the marker-view horizontally
        return -(getWidth() / 2);
    }

    @Override
    public int getYOffset() {
        return 0;
//        // this will cause the marker-view to be above the selected value
//        return -getHeight();
    }
}
