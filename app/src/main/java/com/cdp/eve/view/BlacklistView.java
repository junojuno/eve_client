package com.cdp.eve.view;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cdp.eve.R;
import com.cdp.eve.model.Alarm;
import com.cdp.eve.model.Statis;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import de.hdodenhof.circleimageview.CircleImageView;

@EViewGroup(R.layout.item_blacklist)
public class BlacklistView extends RelativeLayout{

    @ViewById(R.id.civBlacklistImg)
    CircleImageView civBlacklist;

    public BlacklistView(Context context) {
        super(context);
    }

    public void bind(Alarm alarm) {
        Picasso.with(getContext()).load(alarm.getImg())
                .resize(500,500).centerInside().into(civBlacklist);
    }


}
