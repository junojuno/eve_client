package com.cdp.eve.view;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cdp.eve.R;
import com.cdp.eve.widget.EvePhotoImageView;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.item_photo)
public class EvePhotoView extends LinearLayout{

    @ViewById(R.id.ivPhoto)
    EvePhotoImageView ivPhoto;

    public EvePhotoView(Context context) {
        super(context);
    }


    public void bind(Context context, String imgSrc) {
        ivPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Picasso.with(context).load(imgSrc).into(ivPhoto);
    }
}
