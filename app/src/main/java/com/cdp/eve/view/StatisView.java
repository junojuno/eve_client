package com.cdp.eve.view;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cdp.eve.R;
import com.cdp.eve.model.Statis;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.item_statistic)
public class StatisView extends LinearLayout{

    @ViewById(R.id.ivStatisImage)
    ImageView ivImage;

    @ViewById(R.id.ivStatisBookmark)
    ImageView ivBookmark;

    @ViewById(R.id.tvStatisMent)
    TextView tvMent;

    @ViewById(R.id.tvStatisDate)
    TextView tvDate;

    @ViewById(R.id.tvStatisTime)
    TextView tvTime;

    public StatisView(Context context) {
        super(context);
    }

    public void bind(Statis statis) {
        Picasso.with(getContext()).load(statis.getImgSrc()).into(ivImage);
        ivBookmark.setEnabled(statis.isBookmarked());
        tvMent.setText(statis.getMent());
//        tvDate.setText();
//        tvTime.setText();
//        ivImage
    }


}
