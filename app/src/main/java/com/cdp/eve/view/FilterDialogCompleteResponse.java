package com.cdp.eve.view;

/**
 * Created by junojuno on 2015. 8. 14..
 */
public interface FilterDialogCompleteResponse {

//    int selectedLevel;
//
//    int startYear;
//    int startMon;
//    int startDay;
//    int startHour;
//    int startMin;
//
//    int endYear;
//    int endMon;
//    int endDay;
//    int endHour;
//    int endMin;
    void onFilterAdjust(int level,
                        int sY, int sMon, int sD, int sH, int sMin,
                        int eY, int eMon, int eD, int eH, int eMin);
}
