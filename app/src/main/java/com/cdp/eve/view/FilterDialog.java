package com.cdp.eve.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.cdp.eve.R;
import com.cdp.eve.etc.TypefaceManager;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by junojuno on 2015. 8. 13..
 */

public class FilterDialog extends Dialog implements View.OnClickListener {
    private final FilterDialogCompleteResponse response;
    TextView tvFilter;
    TextView tvLevels;
    TextView tvTime;
    TextView tvDate;

    Button btnComplete;

    TextView tvTimeStartHour;
    TextView tvTimeStartMin;
    TextView tvTimeEndHour;
    TextView tvTimeEndMin;
    TextView tvTimeStartDate;
    TextView tvTimeEndDate;

    ImageView ivLevels[];
    ImageView ivLevelBgs[];

    LinearLayout llTimeStart;
    LinearLayout llTimeEnd;

    int selectedLevel;

    int startYear;
    int startMon;
    int startDay;
    int startHour;
    int startMin;

    int endYear;
    int endMon;
    int endDay;
    int endHour;
    int endMin;

    public FilterDialog(Context context, FilterDialogCompleteResponse response) {
        super(context);
        this.response = response;
        selectedLevel = -1;
    }

    void selectLevel(View view) {
        if (selectedLevel != -1) {
            ivLevelBgs[selectedLevel].setVisibility(View.GONE);
        }

        switch (view.getId()) {
            case R.id.civPhotosFilterLv1:
                selectedLevel = 1;
                break;
            case R.id.civPhotosFilterLv2:
                selectedLevel = 2;
                break;
            case R.id.civPhotosFilterLv3:
                selectedLevel = 3;
                break;
            case R.id.civPhotosFilterLv4:
                selectedLevel = 4;
                break;
            case R.id.civPhotosFilterLv5:
                selectedLevel = 5;
                break;
            case R.id.ivPhotosFilterVibration:
                selectedLevel = 6;
                break;

        }

        if (selectedLevel != -1) {
            ivLevelBgs[selectedLevel].setVisibility(View.VISIBLE);
        }

    }

    void setDateTime(View view) {
        GregorianCalendar calendar = new GregorianCalendar();
        int year = calendar.get(Calendar.YEAR);
        int mon = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);

        switch (view.getId()) {
            case R.id.llPhotosFilterTimeStart:
                new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int min) {
                        startHour = hourOfDay;
                        startMin = min;
                        tvTimeStartHour.setText(String.format("%02d", hourOfDay));
                        tvTimeStartMin.setText(String.format("%02d", min));
                    }
                }, hour , min, false).show();
                break;
            case R.id.llPhotosFilterTimeEnd:
                new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int min) {
                        endHour = hourOfDay;
                        endMin = min;
                        tvTimeEndHour.setText(String.format("%02d", hourOfDay));
                        tvTimeEndMin.setText(String.format("%02d", min));
                    }
                }, hour , min, false).show();
                break;

            case R.id.tvPhotosFilterDateStart:
                new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int mon, int day) {
                        mon += 1;
                        startYear = year;
                        startMon = mon;
                        startDay = day;
                        tvTimeStartDate.setText(String.format("%04d-%02d-%02d", year, mon, day));
                    }
                }, year , mon, day).show();
                break;
            case R.id.tvPhotosFilterDateEnd:
                new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int mon, int day) {
                        mon += 1;
                        endYear = year;
                        endMon = mon;
                        endDay = day;
                        tvTimeEndDate.setText(String.format("%04d-%02d-%02d", year, mon, day));
                    }
                }, year , mon, day).show();
                break;
        }
    }

    private void setLayout() {
        Typeface regular = TypefaceManager.getInstance(getContext()).getTypeface(TypefaceManager.TYPEFACE_REGULAR);

        ivLevels = new ImageView[7];
        ivLevelBgs = new ImageView[7];

        ivLevels[1] = (ImageView) findViewById((R.id.civPhotosFilterLv1));
        ivLevels[2] = (ImageView) findViewById((R.id.civPhotosFilterLv2));
        ivLevels[3] = (ImageView) findViewById((R.id.civPhotosFilterLv3));
        ivLevels[4] = (ImageView) findViewById((R.id.civPhotosFilterLv4));
        ivLevels[5] = (ImageView) findViewById((R.id.civPhotosFilterLv5));
        ivLevels[6] = (ImageView) findViewById((R.id.ivPhotosFilterVibration));

        ivLevelBgs[1] = (ImageView) findViewById((R.id.civPhotosFilterBgLv1));
        ivLevelBgs[2] = (ImageView) findViewById((R.id.civPhotosFilterBgLv2));
        ivLevelBgs[3] = (ImageView) findViewById((R.id.civPhotosFilterBgLv3));
        ivLevelBgs[4] = (ImageView) findViewById((R.id.civPhotosFilterBgLv4));
        ivLevelBgs[5] = (ImageView) findViewById((R.id.civPhotosFilterBgLv5));
        ivLevelBgs[6] = (ImageView) findViewById((R.id.ivPhotosFilterVibrationBg));

        tvFilter = (TextView) findViewById(R.id.tvPhotosFilter);
        tvLevels = (TextView) findViewById(R.id.tvPhotosLevels);
        tvTime = (TextView) findViewById(R.id.tvPhotosTime);
        tvDate = (TextView) findViewById(R.id.tvPhotosDate);
        btnComplete = (Button) findViewById((R.id.btnPhotosFilterComplete));

        tvTimeStartHour = (TextView) findViewById(R.id.tvPhotosFilterTimeStartHour);
        tvTimeStartMin = (TextView) findViewById(R.id.tvPhotosFilterTimeStartMin);
        tvTimeEndHour = (TextView) findViewById(R.id.tvPhotosFilterTimeEndHour);
        tvTimeEndMin = (TextView) findViewById(R.id.tvPhotosFilterTimeEndMin);

        tvTimeStartDate = (TextView) findViewById(R.id.tvPhotosFilterDateStart);
        tvTimeEndDate = (TextView) findViewById(R.id.tvPhotosFilterDateEnd);

        llTimeStart = (LinearLayout) findViewById(R.id.llPhotosFilterTimeStart);
        llTimeEnd = (LinearLayout) findViewById(R.id.llPhotosFilterTimeEnd);

        tvFilter.setTypeface(regular);
        tvLevels.setTypeface(regular);
        tvDate.setTypeface(regular);
        tvTime.setTypeface(regular);
        btnComplete.setTypeface(regular);

        tvTimeStartHour.setTypeface(regular);
        tvTimeStartMin.setTypeface(regular);
        tvTimeEndHour.setTypeface(regular);
        tvTimeEndMin.setTypeface(regular);

        tvTimeStartDate.setTypeface(regular);
        tvTimeEndDate.setTypeface(regular);

        ivLevels[1].setOnClickListener(this);
        ivLevels[2].setOnClickListener(this);
        ivLevels[3].setOnClickListener(this);
        ivLevels[4].setOnClickListener(this);
        ivLevels[5].setOnClickListener(this);
        ivLevels[6].setOnClickListener(this);

        llTimeStart.setOnClickListener(this);
        llTimeEnd.setOnClickListener(this);

        btnComplete.setOnClickListener(this);

        tvTimeStartDate.setOnClickListener(this);
        tvTimeEndDate.setOnClickListener(this);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.item_photos_menu_content);
        setLayout();
    }

    @Override
    public void onClick(View v) {
        setDateTime(v);
        selectLevel(v);
        complete(v);
    }

    private void complete(View v) {
        switch (v.getId()) {
            case R.id.btnPhotosFilterComplete:
                response.onFilterAdjust(selectedLevel,startYear,startMon,startDay,
                        startHour,startMin,endYear,endMon,endDay,endHour,endMin);
                dismiss();
                break;
        }
    }
}
